# Commons Host 🐑

This is the command line interface for the [Commons Host](https://commons.host) static site hosting, CDN, and DNS-over-HTTPS (DoH) service.

## Usage

The [commonshost](https://www.npmjs.com/package/commonshost) package is an alias of the [@commonshost/cli](https://www.npmjs.com/package/@commonshost/cli) package.

```
$ npx commonshost
```

... is equivalent to:

```
$ npx @commonshost/cli
```

## Documentation

More information about the Commons Host CLI is available at:

https://help.commons.host/cli/

## See Also

See the [@commonshost](https://www.npmjs.com/org/commonshost) organisation on NPM for more packages. For example:

- [@commonshost/server](https://www.npmjs.com/package/@commonshost/server): The static site server used by the Commons Host CDN, also available as a CLI tool to replicate the production environment during development.

- [@commonshost/manifest](https://www.npmjs.com/package/@commonshost/manifest): The static site dependency tracing tool that generates a Server Push Manifest. This auto-optimises resource loading by eliminating browser-to-server request round trips.
