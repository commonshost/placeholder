#!/usr/bin/env node

const { spawn } = require('child_process')

const command = process.execPath
const args = [
  ...process.execArgv,
  require.resolve('@commonshost/cli'),
  ...process.argv.slice(2)
]
const options = {
  cwd: process.cwd(),
  env: process.env,
  stdio: 'inherit'
}
const child = spawn(command, args, options)
child.on('close', process.exit)
