const test = require('tape')
const { spawnSync } = require('child_process')

test('Passthrough options to @commonshost/cli', (t) => {
  const options = { timeout: 5000, encoding: 'utf8' }
  const child = spawnSync('node', ['index.js', '--help'], options)
  const { stdout, stderr, status } = child
  t.is(status, 0, 'Process succeeds')
  t.is(stderr, '', 'No error output')
  t.ok(/commonshost\/cli/.test(stdout), 'Print usage message')
  t.end()
})
